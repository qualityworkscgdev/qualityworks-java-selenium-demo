package com.qualityworkscg.selenium_demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TimelyLoginTests {

	/*Initialize the appropriate driver to launch our tests, in this case Firefox.*/
	WebDriver driver = new FirefoxDriver();
	
	/*The Webdriver wait is used to make the driver pause for a specified number of seconds, in this case 30s.*/
	WebDriverWait wait = new WebDriverWait(driver,30);
	
	
	WebElement loginButton;
	
	/* The By class represents how the driver will find our web elements, 
	 * the following variable represents the id of the login button.
	 */
	By loginButtonId = new By.ById("loginBtn");
	
	
	/*Here we use the Xpath method to find the element, this xpath searches for a div tag that contains the text 'Timely Tester'*/
	By  userTitleCss = new By.ByXPath("//div[text()='Timely Tester']");
	
	
	
	/* This method will log in to timely as Timely Tester*/ 
	public void login(){
		
		/*Here we tell the driver where it should go to run our test*/
		driver.get("http://timely-qw.herokuapp.com/");
			
		/*Here the login button is set after the driver waits and finds a web element at the specified id*/
		WebElement loginButton = wait.until(ExpectedConditions.visibilityOfElementLocated(loginButtonId));
		
		/*If the login button is successfully found this method will click it.*/
		loginButton.click();
		
		By emailFieldId = new By.ById("email");
		WebElement emailField = wait.until(ExpectedConditions.visibilityOfElementLocated(emailFieldId));
		/*The sendkeys method sends a string to webelement that text my be typed into.*/
		emailField.sendKeys("timelytester@gmail.com");
		
		By passwordFieldId = new By.ById("password");
		WebElement passwordField = wait.until(ExpectedConditions.visibilityOfElementLocated(passwordFieldId));
		passwordField.sendKeys("Password123");
		
		By submitButtonCss = new By.ByCssSelector("button[type='submit");
		WebElement submitButton = wait.until(ExpectedConditions.visibilityOfElementLocated(submitButtonCss));
		submitButton.click();
		
	}
	
	/* The @Test annotation tells TestNG that this method should be executed as a TestNG test method */
	@Test(description="Login to timely as Timely Tetser")
	public void loginToTimely(){
		
		/* Here the driver is started as a firefox driver which will use the firefox browser*/
		//driver = new FirefoxDriver();
		
		/*This method runs the procedures to log into timely*/
		login();
		
		WebElement userTitle = wait.until(ExpectedConditions.visibilityOfElementLocated(userTitleCss));
		
		/* Here we check to ensure that the title 'Timely Tester' is visible on the page. */
		Assert.assertTrue(userTitle.isDisplayed());
		
		/*Here we quit the driver, this closes our selected web browser */
		driver.quit();
	}
	
	
	@Test(description="Login to timely as Timely Tester then log out")
	public void logOutTimely(){
		
		//driver = new FirefoxDriver();
		
		/*First we have to login to timely*/
		login();
		
		
		By logoutLinkId = new By.ById("btnLogout");
		WebElement logoutLink = wait.until(ExpectedConditions.visibilityOfElementLocated(logoutLinkId));
		logoutLink.click();
		
		By confirmLogoutCss = new By.ByCssSelector("div[class='ui green ok inverted button']");
		WebElement confirmLogout = wait.until(ExpectedConditions.visibilityOfElementLocated(confirmLogoutCss));
		confirmLogout.click();
		
		Assert.assertTrue(loginButton.isDisplayed());
		
		driver.quit();
		
	}
}
